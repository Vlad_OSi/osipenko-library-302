<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Books</title>
</head>
<body>
<a href="/osipenko_library_302_war">Main</a>
<form action="">
    <input type="text" name="value">
    <select name="type">
        <option value="name" selected>Name</option>
        <option value="genre">Genre</option>
        <option value="author">Author</option>
        <option value="mark">Mark</option>
    </select>
</form>
<table align="center">
    <tr><th>Id</th>
    <th>Name</th>
    <th>Author</th>
    <th>Genre</th>
    <th>Mark</th>
    <th></th></tr>
    <c:forEach items="${books}" var="book">
        <tr>
            <td>${book.getId()}</td>
            <td>${book.getName()}</td>
            <td>${book.getAuthor()}</td>
            <td>${book.getGenre().getName()}</td>
            <td>${book.getMark()}</td>
            <td><a href="./books/delete/${book.getId()}">Delete</a></td>
            <td><a href="./books/update/${book.getId()}">Update</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
