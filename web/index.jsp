<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>


  <div>
    <a href="./books">See books</a>
    <br>
    <a href="./genres">See genres</a>
    <br>
    <a href="./genres/create">Add genre</a>
    <br>
    <a href="./books/create">Add book</a>
  </div>
  <sec:authorize access="isAuthenticated()">
    <p>Ваш логин: <sec:authentication property="principal.username" /></p>
    <p><a class="btn btn-lg btn-danger" href="logout" role="button">Выйти</a></p>

  </sec:authorize>
  </body>
</html>
