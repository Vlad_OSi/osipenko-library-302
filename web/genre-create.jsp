<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add book</title>
</head>
<body>
<p>${validation}</p><br/>
<form action="" method="post">
    <a href="/osipenko_library_302_war">Main</a>
    <table align="center">
        <tr><td>Name</td><td>
    <input type="text" name="name" value="${genre.getName()}" required></td></tr>
        <tr><td>About</td><td>
            <textarea name="about" cols="30" rows="10" required>${genre.getAbout()}</textarea></td></tr>
        <tr><td></td><td><input type="submit" value="Create"></td></tr>
    </table>
</form>
</body>
</html>
