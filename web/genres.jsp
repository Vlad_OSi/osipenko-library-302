<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Books</title>
</head>
<body>
<a href="/osipenko_library_302_war">Main</a>
<table align="center">
    <tr>
    <th>Name</th>
    <th>About</th></tr>
    <c:forEach items="${genres}" var="genre">
        <tr>
            <td>${genre.getName()}</td>
            <td>${genre.getAbout()}</td>
            <td><a href="./genres/delete/${genre.getId()}">Delete</a></td>
            <td><a href="./genres/update/${genre.getId()}">Update</a></td>
        </tr>

    </c:forEach>
</table>
</body>
</html>
