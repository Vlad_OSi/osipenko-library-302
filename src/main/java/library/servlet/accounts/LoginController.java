package library.servlet.accounts;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.ejb.Remove;
import javax.ejb.Singleton;

@Singleton
@Controller
@RequestMapping("/login")
public class LoginController {

    @Remove
    @RequestMapping(method = RequestMethod.GET)
    public String loginPage(Model model){
        return "/login.jsp";
    }

}