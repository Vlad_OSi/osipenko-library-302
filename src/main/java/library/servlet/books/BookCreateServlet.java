/*
package library.servlet.books;

import library.dao.jdbc.BookDao;
import library.dao.jdbc.GenreDao;
import library.model.Book;
import library.model.Genre;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@WebServlet(name = "BookCreateServlet", urlPatterns = {"/old/books/create"})
public class BookCreateServlet extends HttpServlet {

    @Inject
    private BookDao bookDao;

    @Inject
    private GenreDao genreDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Book book = new Book();
        book.setName(request.getParameter("name"));
        book.setAuthor(request.getParameter("author"));
        Genre genre = new Genre();
        genre.setId(Integer.parseInt(request.getParameter("genre")));
        book.setGenre(genre);
        book.setMark(Integer.parseInt(request.getParameter("mark")));
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(book);
        if (constraintViolations.size() > 0) {
            request.setAttribute("name", book.getName());
            request.setAttribute("author", book.getAuthor());
            request.setAttribute("mark", book.getMark());
            request.setAttribute("validation", constraintViolations.stream().map(s->s.getMessage()).collect(Collectors.toSet()));
            doGet(request,response);
        }
        try {
            bookDao.insertBook(book);
            response.sendRedirect(request.getContextPath() + "/books");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Genre> genres = genreDao.readGenres();
            request.setAttribute("genres", genres);

            getServletContext().getRequestDispatcher("/book-create.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
*/
