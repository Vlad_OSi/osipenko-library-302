package library.servlet.books;

import library.model.Book;
import library.model.Genre;
import library.serices.BookService;
import library.serices.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Asynchronous
@Stateless
@Controller
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    private final GenreService genreService;

    @Autowired
    public BookController(BookService bookService, GenreService genreService) {
        this.bookService = bookService;
        this.genreService = genreService;
    }

    @GetMapping
    public String get(Model model, @ModelAttribute String value,
                      @ModelAttribute String type) {
        List<Book> books;
        if (value == null || value.isEmpty()) {
            books = bookService.getAll();
        } else {
            books = bookService.getAllByName(type, value);
        }
        model.addAttribute("books", books);
        return "/list.jsp";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable Integer id) {
        List<Genre> genres = genreService.getAll();
        model.addAttribute("genres", genres);
        Book book = bookService.getById(id);
        model.addAttribute("book", book);
        return "/book-update.jsp";
    }

    @PostMapping("/update/{id}")
    public String update(Model model,@PathVariable Integer id, @ModelAttribute Book book) {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(book);
        if (constraintViolations.size() > 0) {
            model.addAttribute("validation", constraintViolations.stream().map(s -> s.getMessage()).collect(Collectors.toSet()));
            return "/book-update.jsp";
        }
        book.setId(id);
        bookService.createOrUpdate(book);
        return "redirect: /osipenko_library_302_war/books";
    }

    @GetMapping("/delete/{id}")
    public String delete(Model model, @PathVariable Integer id) {
        bookService.delete(id);
        return "redirect: /osipenko_library_302_war/books";
    }

    @PostMapping("/create")
    protected String create(Model model, @ModelAttribute Book book) {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(book);
        if (constraintViolations.size() > 0) {
            model.addAttribute("book", book);
            model.addAttribute("validation", constraintViolations.stream().map(s -> s.getMessage()).collect(Collectors.toSet()));
            return "/create.jsp";
        }
        bookService.createOrUpdate(book);
        return "redirect: /osipenko_library_302_war/books";

    }

    @GetMapping("/create")
    protected String create(Model model) {
        List<Genre> genres = genreService.getAll();
        model.addAttribute("genres", genres);
        return "/book-create.jsp";
    }
}
