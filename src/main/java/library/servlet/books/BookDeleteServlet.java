/*
package library.servlet.books;

import library.dao.jdbc.BookDao;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "BookDeleteServlet", urlPatterns = {"/old//books/delete"})
public class BookDeleteServlet extends HttpServlet {

    @Inject
    private BookDao bookDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            bookDao.deleteBookById(Integer.parseInt(request.getParameter("id")));
            response.sendRedirect(request.getContextPath() + "/books");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
*/
