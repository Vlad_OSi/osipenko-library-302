/*
package library.servlet.genres;

import library.dao.jdbc.GenreDao;
import library.model.Genre;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@WebServlet(name = "GenreUpdateServlet", urlPatterns = {"/old/genres/update"})
public class GenreUpdateServlet extends HttpServlet {

    @Inject
    private GenreDao genreDao;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Genre genre = new Genre();

        genre.setId(Integer.parseInt(request.getParameter("id")));
        genre.setName(request.getParameter("name"));
        genre.setAbout(request.getParameter("about"));

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Genre>> constraintViolations = validator.validate(genre);
        if (constraintViolations.size() > 0) {
            request.setAttribute("id", genre.getId());
            request.setAttribute("validation",constraintViolations.stream().map(s->s.getMessage()).collect(Collectors.toSet()));
            doGet(request,response);
        }
        try {
            genreDao.updateGenre(genre);
            response.sendRedirect(request.getContextPath() + "/genres");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            List<Genre> genres = genreDao.readGenres();
            request.setAttribute("genres", genres);
            Genre genre = genreDao.readGenreById(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("genre", genre);
            getServletContext().getRequestDispatcher("/genre-update.jsp").forward(request, response);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
*/
