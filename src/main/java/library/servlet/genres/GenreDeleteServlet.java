/*
package library.servlet.genres;

import library.dao.jdbc.GenreDao;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name = "GenreDeleteServlet", urlPatterns = {"/old/genres/delete"})
public class GenreDeleteServlet extends HttpServlet {

    @Inject
    private GenreDao genreDao;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            genreDao.deleteGenreById(Integer.parseInt(request.getParameter("id")));
            response.sendRedirect(request.getContextPath() + "/genres");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
*/
