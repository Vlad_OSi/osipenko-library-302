package library.serices;

import library.dao.GenreDao;
import library.model.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ejb.Remove;
import javax.ejb.Singleton;
import java.util.List;

@Singleton
@Service
public class GenreService {

    private final GenreDao genreDao;

    @Autowired
    public GenreService(GenreDao genreDao) {
        this.genreDao = genreDao;
    }


    @Transactional(readOnly = true)
    public Genre getById(int id) {
        return genreDao.getById(id);
    }

    @Transactional
    public List<Genre> getAll() {
        return genreDao.getAll();
    }

    @Remove
    @Transactional
    public void delete(int id) {
        genreDao.delete(id);
    }

    @Remove
    @Transactional
    public void createOrUpdate(Genre genre) {
        genreDao.updateOrCreate(genre);
    }

}
