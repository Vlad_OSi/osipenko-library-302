package library.serices;

import library.dao.BookDao;
import library.dao.GenreDao;
import library.model.Book;
import library.model.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ejb.Singleton;
import java.util.List;

@Singleton
@Service
public class BookService {


    private final BookDao bookDao;

    @Autowired
    public BookService(BookDao bookDao) {
        this.bookDao = bookDao;
    }


    @Transactional(readOnly = true)
    public Book getById(int id) {
        return bookDao.getById(id);
    }

    @Transactional(readOnly = true)
    public List<Book> getAll() {
        return bookDao.getAll();
    }

    @Transactional
    public void delete(int id) {
        bookDao.delete(id);
    }

    @Transactional
    public void createOrUpdate(Book book) {
        bookDao.updateOrInsert(book);
    }

    @Transactional(readOnly = true)
    public List<Book> getAllByName(String param, String value){
        return bookDao.getByFields(param,value);
    }

}
