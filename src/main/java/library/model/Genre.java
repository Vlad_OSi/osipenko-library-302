package library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;


@Entity
@Table(name = "Genres")
public class Genre implements Comparable<Genre> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    @Pattern(regexp = "[A-Z][a-z ]*", message = "Bad reg exp")
    @Column(name = "name")
    private String name;

    @Size(min = 10, max = 200, message = "About should be from 10 tot 200 symbols")
    @Column(name = "about")
    private String about;

    @Transient
    private SortedSet<Genre> genres;

    public Genre(String name, String about, int id) {
        this.name = name;
        this.about = about;
        this.id = id;
    }

    public Genre() {

    }

    public Genre(Collection<Genre> genres) {

        this.genres = new TreeSet(genres);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public boolean removeGenre(Genre g) {
        if (genres.contains(g)) {

            genres.remove(g);

            return true;
        } else return false;
    }

    public Collection<Genre> getListOfGenres(Comparator<? super Genre> comparator) {
        List<Genre> g = new ArrayList(genres);
        g.sort(comparator);
        System.out.println(name + ":");
        g.forEach(b -> System.out.println(b));
        return g;
    }

    @Override
    public int compareTo(Genre o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return name + " - " + about + " ";
    }


}
