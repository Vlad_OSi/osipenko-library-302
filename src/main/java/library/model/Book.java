package library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "books")
public class Book implements Comparable<Book> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @ManyToOne(targetEntity = Genre.class)
    @JoinColumn(referencedColumnName = "id", name = "genre_id")
    private Genre genre;

    @Size(min=2,max=200,message = "Author should contains from 2 to 200 chars")
    @Column(name = "author")
    private String author;

    @NotNull
    @Min(value = 0, message = "Min is 0")
    @Max(value = 5, message = "Max for mark is 5")
    @Column(name = "mark")
    private int mark;

    public Book() {

    }
    public Book(int id, String name, Genre genre, String author, int mark) {
        this.id = id;
        this.name = name;
        this.genre = genre;
        this.author = author;
        this.mark = mark;

    }

    public Book(int mark) {
        this.id = id;

    }

    @Override
    public boolean equals(Object obj) {
        return id == ((Book) obj).id;
    }

    @Override
    public String toString() {
        return id + "   " + name + "   " + genre + "   " + author + " " + mark + "  ";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getMark() {
        return mark;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getAuthor() {
        return author;
    }


    public void setId(int id) {
        this.id = id;
    }
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public int compareTo(Book o) {
        return id - o.id;
    }
}
