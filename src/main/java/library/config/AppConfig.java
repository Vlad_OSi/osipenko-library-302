package library.config;

import library.dao.UserDao;
import library.serices.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.sql.SQLException;

@Configuration
@EnableWebMvc
public class AppConfig {
    @Autowired
    private UserDao userDao;

    @Bean
    public UserDetailsService getUserDetailsService() throws SQLException, ClassNotFoundException {
        return new UserService(userDao);
    }
}
