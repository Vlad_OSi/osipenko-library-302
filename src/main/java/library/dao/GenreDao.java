package library.dao;

import library.model.Genre;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Stateful
@StatefulTimeout(value = 30, unit = TimeUnit.SECONDS)
@Repository
public class GenreDao {

    private final SessionFactory sessionFactory;

    @Autowired
    public GenreDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void delete(int id) {
        Genre genre = new Genre();
        genre.setId(id);
        getSession().delete(genre);
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void updateOrCreate(Genre genre) {
        getSession().saveOrUpdate(genre);
    }

    public Genre getById(int id) {
        return getSession().get(Genre.class, id);
    }

    public List<Genre> getAll() {
        return getSession().createQuery("SELECT g FROM Genre g")
                .getResultList();
    }


}
